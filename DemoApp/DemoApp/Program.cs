﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DemoApp
{
    class Program
    {
        private static IFooBar fooBarProvider;
        private static IFooBar secondFooBarProvider;

        static void Main(string[] args)
        {
            Console.WriteLine("Foobar!");

            //fooBarProvider = new FooBar();
            secondFooBarProvider = new FooBar();
            for (uint i = 0; i < 100; i++)
            {
                fooBarProvider.PrintFooBar(i);
            }

            for (uint i = 100; i < 200; i++)
            {
                secondFooBarProvider.PrintFooBar(i);
            }

            Console.WriteLine("Donezo");
            Console.ReadLine();
        }
    }
}
