﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoApp
{
    public class FooBar : IFooBar
    {
        public FooBar() { }

        public void PrintFooBar(uint input)
        {
            string result = "";
            if (input % 5 == 0)
                result += "Foo";
            if (input % 7 == 0)
                result += "Bar";

            if (string.IsNullOrEmpty(result))
                result = input.ToString();

            Console.WriteLine(result);
        }
    }
}
